#include <stdio.h>
#include <vector>
#include <queue>

using namespace std;

int main()
{
	vector<int> graf[100];
	int pred[100];
	int levels[100];

	int n, m;
	int a, b;
	int start;

	FILE *f = fopen("a.txt", "r");
	fscanf(f,"%d", &n);
	fscanf(f,"%d", &m);
	fscanf(f,"%d", &start);

	--start;

	for(int i=0; i<m; ++i)
	{
		fscanf(f, "%d", &a);
		fscanf(f, "%d", &b);
		graf[a-1].push_back(b-1);
		graf[b-1].push_back(a-1);
	}
	fclose(f);

	for(int i=0; i<n; ++i)
	{
		pred[i] = -1;
		levels[i] = 0;
	}
	levels[start] = 1;

	queue<int> q;

	q.push(start);
	while( !q.empty() )
	{
		int t = q.front();
		q.pop();

		for(int i=0; i<graf[t].size(); ++i) {
			int c = graf[t][i];
			if( levels[c] == 0 )
			{
				q.push(c);

				pred[c]=t;
				levels[c] = levels[t] + 1;
			}
		}
	}

	for(int i=0; i<n; ++i)
		printf("%d ", levels[i]);
	printf("\n");

	for(int i=0; i<n; ++i)
		printf("%d ", pred[i]+1);
	printf("\n");

	return 0;
}
